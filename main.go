package main

import (
	"bytes"
	"log"
	"net/http"
	"time"
)

//var TIME_SLEEP_LATENCY int = 5

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {

	forever := make(chan bool)

	go func() {
		for {
			log.Printf("Update Availability")
			//1. Prendo la lista dei pod dal master (e vedere come )
			//   o da un eventuale service registry
			podList := GetAllMetadata()

			//2. Stimo la disponibilità rispetto ai servizi ottenuti
			a := make([]float64, len(podList))
			for i, _ := range podList {
				a[i] = estimateAvailability(podList[i].Ip, podList[i].Availability)
			}
			for i, _ := range podList {
				podList[i].Availability = a[i]
			}

			//5.  Faccio l'update su CASSANDRA
			_, err := http.Post(pathToClusterMonitorPut, "application/json; charset=UTF-8",
				bytes.NewBuffer(StructToJson(podList)))
			if err != nil {
				// handle error
				panic(err)
			}
			time.Sleep(time.Second * 30) //TIME_SLEEP_LATENCY)
		}
	}()

	log.Printf("Start to receive Latency info")

	<-forever
}
