package main

import (
	gocql "github.com/gocql/gocql"
)

type MetaPod struct {
	UUid       gocql.UUID `json:"uuid"`
	Uid        string     `json:"uid"`
	Type       string     `json:"type"`
	Ip         string     `json:"ip"`
	Node       string     `json:"node"`
	Deployment string     `json:"deployment"`
	//Availability float64
	//Speedup      float64
}
type MetaPodAvailability struct {
	MetaPod
	Availability float64 `json:"availability"`
	//Speedup      float64
}

func makeMetaPod(uuid gocql.UUID, uid string, t string, ip string, node string, deployment string) MetaPod {
	return MetaPod{
		UUid:       uuid,
		Uid:        uid,
		Type:       t,
		Ip:         ip,
		Node:       node,
		Deployment: deployment,
	}
}
func makeMetaPodAvailability(metapod MetaPod, availability float64) MetaPodAvailability {
	return MetaPodAvailability{
		MetaPod:      metapod,
		Availability: availability,
	}
}

type MetaPods []MetaPod

type RegistratedEntity struct {
	Response   bool       `json:"response"`
	UUid       gocql.UUID `json:"uuid"`
	Uid        string     `json:"uid"`
	Type       string     `json:"type"`
	Ip         string     `json:"ip"`
	Node       string     `json:"node"`
	Deployment string     `json:"deployment"`
}

func makeRegistratedEntity(response bool, uuid gocql.UUID, uid string, t string, ip string,
	node string, deployment string) RegistratedEntity {
	return RegistratedEntity{
		Response:   response,
		UUid:       uuid,
		Uid:        uid,
		Type:       t,
		Ip:         ip,
		Node:       node,
		Deployment: deployment,
	}
}
