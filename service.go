package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func GetAllMetadata() []MetaPodAvailability {
	var podlist []MetaPodAvailability
	fmt.Println(pathToClusterMonitorGet)
	resp, err := http.Get(pathToClusterMonitorGet)
	if err != nil {
		// handle error
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &podlist); err != nil {
		panic(err)
	}
	return podlist
}

func estimateAvailability(ip string, availability float64) float64 {
	var a float64
	url := "http://" + ip + ":8082/getHello"
	resp, err := http.Get(url)
	if err != nil {
		//panic(err)
		a = (0.0-availability)*0.1 + availability
		return a
	}
	if resp.StatusCode == http.StatusOK {
		a = (1.0-availability)*0.1 + availability
	} else {
		a = (0.0-availability)*0.1 + availability
	}
	return a
}
